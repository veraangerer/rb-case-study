# Case Study

## Assignment

*Imagine we received the following message from our Product Owner: "Hey everyone, I need a new service that allows me to update and retrieve information about our solar system. I'm especially interested in details about our planets (e.g. name, size, distance to the sun). Feel free to add more information if you think it makes sense. The service is going to be consumed by an application maintained by another team, so please document the interface. Last but not least, we need a way to retrieve the distance between two planets. Could you please add this capability to the service?". Now it is your job to build a new (micro)service satisfying the requirements requested above.*

*Please use Javascript (Typescript would be a bonus) and feel free to use libraries or write the application in VanillaJs; whatever works for you is fine. Moreover, I'd like to ask you to pretend you were working in a team; create and approve your own Pull Requests. You don't need to actually deploy it on a server, but we’re interested in how you would build and ship it. Please make sure that it could run on one of the large cloud platforms (AWS/GCP/Azure).*

Source: copied from assignment email

## Commits
 - based on [Gitmoji](https://gitmoji.carloscuesta.me/)
 - Application based on [Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Add data
```bash
# install mongodb
$ brew tap mongodb/brew
$ brew install mongodb-community@4.4
# start mongodb
$ brew services start mongodb-community@4.4
# start mongodb session in shell
$ mongo
# install mongodb-compass as a client
$ brew cask install mongodb-compass
```

- follow installation procedure
- start MongoDB Compass
- add database ("nest" / collection name "planets") by importing `planets.json` file
- or add it via import using command line statements in mongo session

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Access the api via graphql playground

```bash
http://localhost:3000/graphql
```

## Test

```bash
# unit tests
$ npm test
```

## Build

```bash
$ npm build
```

## Api Documentation
access via graphql playground "Docs" option

![Playground](playground.png)

