
import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { Planet } from './models/planet.model'
import { PlanetService } from './planets.service'
import { PlanetUpdateInput } from './dto/planet-update.input'
import { PlanetCreateInput } from './dto/planet-create.input'

@Resolver(() => Planet)
export class PlanetsResolver {
  constructor(
    private planetsService: PlanetService
  ) {}

  @Query(() => Planet)
  getPlanet(@Args('id', { type: () => String }) id: string): Promise<Planet>  {
    return this.planetsService.findById(id)
  }

  @Query(() => [Planet])
  getPlanets(): Promise<Planet[]> {
    return this.planetsService.findAll()
  }

  @Mutation(() => Planet)
  createPlanet(@Args('data') data: PlanetCreateInput): Promise<Planet> {
    return this.planetsService.create(data)
  }

  @Mutation(() => Planet)
  updatePlanet(
    @Args('id', { type: () => String }) id: string,
    @Args('data') data: PlanetUpdateInput
  ): Promise<Planet> {
    return this.planetsService.update(id, data)
  }

  @Mutation(() => Boolean)
  removePlanet(@Args('id', { type: () => String }) id: string): Promise<Boolean> {
    return this.planetsService.delete(id)
  }

  @Query(() => Number)
  getDistance(
    @Args('planetId1', { type: () => String }) id: string,
    @Args('planetId2', { type: () => String }) id2: string): number {
    return this.planetsService.computeDistanceBetween(id, id2)
  }
}