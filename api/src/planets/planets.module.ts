import { Module } from '@nestjs/common';
import { PlanetsResolver } from './planets.resolver';
import { PlanetService } from './planets.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Planet, PlanetSchema } from './models/planet.model';

@Module({
  imports: [
    Planet,
    MongooseModule.forFeature([{ name: Planet.name, schema: PlanetSchema }])
  ],
  providers: [PlanetsResolver, PlanetService],
})
export class PlanetsModule {}