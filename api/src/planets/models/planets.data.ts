// taken from https://gist.github.com/IDdesigner/25073bdf92b8bcae7c0b224bb0decb2e
export const planets = [
    {
        id: 'mercury',
        name: 'Mercury',
        moons: 0,
        diameter: 4879,
        distanceFromSun: 57.9
    },
    {
        id: 'venus',
        name: 'Venus',
        moons: 0,
        diameter: 12104,
        distanceFromSun: 108.2
    },
    {
        id: 'earth',
        name: 'Earth',
        moons: 1,
        diameter: 12756,
        distanceFromSun: 149.6
    },
    {
        id: 'mars',
        name: 'Mars',
        moons: 0,
        diameter: 6792,
        distanceFromSun: 227.9
    },
    {
        id: 'jupiter',
        name: 'Jupiter',
        moons: 67,
        diameter: 142984,
        distanceFromSun: 778.6
    },
    {
        id: 'saturn',
        name: 'Saturn',
        moons: 62,
        diameter: 120536,
        distanceFromSun: 1433.5
    },
    {
        id: 'uranus',
        name: 'Uranus',
        moons: 27,
        diameter: 51118,
        distanceFromSun: 2872.5
    },
    {
        id: 'neptune',
        name: 'Neptune',
        moons: 14,
        diameter: 49528,
        distanceFromSun: 4495.1
    },
    {
        id: 'pluto',
        name: 'Pluto',
        moons: 5,
        diameter: 2370,
        distanceFromSun: 5906.4
    }
]