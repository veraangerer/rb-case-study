import { Field, Float, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@ObjectType()
@Schema()
export class Planet {
    @Prop({ required: true })
    @Field(() => String)
    id: string;

    @Prop({ required: true })
    @Field({ nullable: false })
    name: string;

    @Prop({ required: true })
    @Field({ nullable: false })
    moons: number;

    @Prop()
    @Field(() => Float)
    distanceFromSun: number;

    @Prop()
    @Field(() => Float)
    diameter: number;
}

export const PlanetSchema = SchemaFactory.createForClass(Planet);

export type PlanetDocument = Planet & Document;
