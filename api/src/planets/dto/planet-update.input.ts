
import { IsString, IsOptional, IsNumber} from 'class-validator';
import { Field, Float, InputType } from '@nestjs/graphql'

@InputType()
export class PlanetUpdateInput {
    @IsString()
    @IsOptional()
    @Field(() => String, { nullable: true })
    name: string;

    @IsNumber()
    @IsOptional()
    @Field(() => Float, { nullable: true })
    moons: number;

    @IsNumber()
    @IsOptional()
    @Field(() => Float)
    @Field(() => Float, { nullable: true })
    distanceFromSun: number;

    @IsNumber()
    @IsOptional()
    @Field(() => Float, { nullable: true })
    diameter: number;
}