
import { IsString, IsNumber} from 'class-validator';
import { Field, Float, InputType } from '@nestjs/graphql'

@InputType()
export class PlanetCreateInput {
    @IsString()
    @Field(() => String, { nullable: false })
    id: string;

    @IsString()
    @Field(() => String, { nullable: false })
    name: string;

    @IsNumber()
    @Field(() => Float, { nullable: false })
    moons: number;

    @IsNumber()
    @Field(() => Float, { nullable: true })
    distanceFromSun: number;

    @IsNumber()
    @Field(() => Float, { nullable: true })
    diameter: number;
}