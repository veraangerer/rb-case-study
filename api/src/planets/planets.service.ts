import { Injectable } from '@nestjs/common';
import { planets } from './models/planets.data'
import { InjectModel } from '@nestjs/mongoose';
import { Planet, PlanetDocument } from './models/planet.model';
import { Model } from 'mongoose';

@Injectable()
export class PlanetService {
  constructor(@InjectModel(Planet.name) private planetModel: Model<PlanetDocument>) {}

  computeDistanceBetween(id1, id2): number {
    const { distanceFromSun: distance1 } = planets.find(p => p.id === id1);
    const { distanceFromSun: distance2 } = planets.find(p => p.id === id2);

    return Math.abs(distance1 - distance2)
  }

  async findAll(): Promise<Planet[]> {
    return this.planetModel.find().exec();
  }

  async findById(id): Promise<Planet> {
    return this.planetModel.findOne({id}).exec();
  }

  async update(id, data): Promise<Planet> {
    return this.planetModel.findOneAndUpdate({id}, data).exec();
  }

  async create(data): Promise<Planet> {
    return this.planetModel.create(data)
  }

  async delete(id): Promise<Boolean>{
    const { deletedCount } = await this.planetModel.deleteOne({id}).exec()
    return deletedCount === 1
  }
}