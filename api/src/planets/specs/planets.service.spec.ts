
import { PlanetService } from './../planets.service';
import { Planet } from '../models/planet.model';
import {  getModelToken} from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';

const planets = [
  {
    id: 'mercury',
    name: 'Mercury',
    moons: 0,
    diameter: 4879,
    distanceFromSun: 57.9
  },
  {
    id: 'venus',
    name: 'Venus',
    moons: 0,
    diameter: 12104,
    distanceFromSun: 108.2
  },
]  

describe('PlanetService', () => {
  let planetsService: PlanetService;

  beforeEach(async () => {
    
    const PlanetModelMock = () => ({
      create: jest.fn(),
      save: jest.fn(),
      remove: jest.fn(),
      find: jest.fn(),
      findAll: jest.fn()
    })

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PlanetService,
        {
          provide: getModelToken(Planet.name),
          useValue: PlanetModelMock,
        },
      ],
    }).compile();

    planetsService = module.get(PlanetService);
  });

  describe('computeDistanceBetween', () => {
    it('computes the distance between two different planets', () => {
      const [planet1, planet2] = planets

      expect(planetsService.computeDistanceBetween(planet1.id, planet2.id)).toEqual(50.300000000000004);
    });

    it('computes 0 as the distance between the same planet', () => {
      const [planet] = planets
      const { id } = planet

      expect(planetsService.computeDistanceBetween(id, id)).toEqual(0);
    });
  });
});